# template.sh - edit this file
function usage
{
  echo "usage: $0 arguments ..."
  if [ $# -eq 1 ]
  then echo "ERROR: $1"
  fi
}

# Your script starts after this line.
if [ $# -lt  1 ]
then
	usage
else
	echo "Bohdan Hrytsak"
	date
	echo " "
	while [ $# -gt 0 ]
	do 
		case $1 in
		"TestError")
		     usage "TestError fount"
		     ;;
		"now")
		     echo "It is now $( date +"%r")"
		     ;;
	        *)
		     usage "Do not know what to do with $1"
	    	     ;;
	        esac
   		echo "*****"
		shift
	done
fi	
